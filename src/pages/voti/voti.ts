import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import {ApiProvider} from '../../providers/api/api';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-voti',
  templateUrl: 'voti.html',
  providers: [ApiProvider]
})
export class VotiPage {
    email: string
    password: string

  constructor(public navCtrl: NavController, public apivoti:ApiProvider, private storage: Storage) {
  }
  ionViewDidLoad() {
    console.log("Data loaded");
    this.storage.get('username').then((val: string) => {
        console.log(val)
        this.email = val
      });
    this.storage.get('password').then((val: string) => {
        this.password = val
        console.log(val)
      });
  }
  postRequest(email = this.email, password = this.password) 
  {
      console.log(email, password)
      this.apivoti.voti(email, password)
          .subscribe((response) => {
              alert(response);
              console.log('Ricevuta una risposta')
          }, (error) => {
              alert(error);
          });
  }
}
