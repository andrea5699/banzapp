import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VotiPage } from '../voti/voti';
import { LezionePage } from '../lezione/lezione';
import { ImpostazioniPage } from '../impostazioni/impostazioni';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {

  tab1Root: any = VotiPage;
  tab2Root: any = LezionePage;
  tab3Root: any = ImpostazioniPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,) {
  }
  goToVoti(username=this.navParams.get('username'), password=this.navParams.get('password')){
    this.navCtrl.push(VotiPage, {
      username: username,
      password: password})
      console.log(username)
  }
}
