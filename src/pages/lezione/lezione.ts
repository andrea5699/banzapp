import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-lezione',
  templateUrl: 'lezione.html',
  providers: [ApiProvider]
})
export class LezionePage {
    email: string
    password: string

  constructor(public navCtrl: NavController, public apilezione:ApiProvider, private storage: Storage) {
  }
  ionViewDidLoad() {
    console.log("Data loaded");
    this.storage.get('username').then((val: string) => {
        console.log(val)
        this.email = val
      });
    this.storage.get('password').then((val: string) => {
        this.password = val
        console.log(val)
      });
  }
  postRequest(email = this.email, password = this.password) 
  {
      this.apilezione.lezione(email, password)
          .subscribe((response) => {
              alert(response);
              console.log('Ricevuta una risposta')
          }, (error) => {
              console.log(error)
              alert(error);
          });
  }
}
