import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsControllerPage } from '../tabs-controller/tabs-controller';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  private login : FormGroup;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, private storage: Storage) {
    this.login = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  setData(){
    this.storage.set('username', this.login.value.username)
    this.storage.set('password', this.login.value.password)
    console.log('Data set')
  }
  goToVoti(){
    this.navCtrl.push(TabsControllerPage);
    this.setData();
  }
}

    