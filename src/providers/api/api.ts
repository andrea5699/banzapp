import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiProvider {

  constructor(public http: Http) {
    console.log('Provider lanciato');
  }
  voti(email, password) {       
      let urlSearchParams = new URLSearchParams();
      urlSearchParams.append('username', email);
      urlSearchParams.append('password', password);
      let body = urlSearchParams.toString()
  
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  
      return this.http
          .post('https://classevivaapi.herokuapp.com/voti', body, { headers: headers })
          .map(response => response.json())
  }
  lezione(email: string, password: string) {       
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append('username', email);
    urlSearchParams.append('password', password);
    let body = urlSearchParams.toString()

    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return this.http
        .post('https://classevivaapi.herokuapp.com/lezioneoggi', body, { headers: headers })
        .map(response => response.json())
}
}
