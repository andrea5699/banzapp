import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';

import { VotiPage } from '../pages/voti/voti';


import { LoginPage } from '../pages/login/login';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    firebase.initializeApp({
      apiKey: "AIzaSyDEA4iRK7Nx6Scn_aABhvprKgzGLUBQYQI",
      authDomain: "banzapp-firebase.firebaseapp.com",
      databaseURL: "https://banzapp-firebase.firebaseio.com",
      projectId: "banzapp-firebase",
      storageBucket: "banzapp-firebase.appspot.com",
      messagingSenderId: "595081945697"
    });
  }
  goToLogin(params){
    if (!params) params = {};
    this.navCtrl.setRoot(LoginPage);
  }goToVoti(params){
    if (!params) params = {};
    this.navCtrl.setRoot(VotiPage);
  }
}
