import os
import re
import requests
from bs4 import BeautifulSoup
from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


def login(username, password):
    """Effettua il login su ClasseViva"""
    session = requests.Session()

    url = "https://web.spaggiari.eu/auth/app/default/AuthApi4.php?a=aLoginPwd"

    params = {'uid': username, 'pwd': password}

    session.post(url, data=params)

    return session

@app.route('/lezioneoggi', methods=['GET', 'POST'])
def lezioneoggi():
    """Scraping e parsing della lezione del giorno"""
    req = login(request.form['username'], request.form['password']).get("https://web.spaggiari.eu/cvv/app/default/regclasse.php")

    soup = BeautifulSoup(req.content, "lxml")
    lezione = soup.find_all(
        'table', attrs={'class': 'griglia_tab', 'id': 'data_table'})[2]
    lezioneparsed=re.sub(r'\n\s*\n', '\n', lezione.text.strip())
    if not lezione:
        print("Tabella vuota o non trovata")
    return jsonify(lezioneparsed)

@app.route('/voti', methods=['GET', 'POST'])
def voti():
    """Scraping e parsing dei voti"""
    req = login(request.form['username'], request.form['password']).get("https://web.spaggiari.eu/cvv/app/default/genitori_voti.php")

    soup = BeautifulSoup(req.content, "lxml")
    data = []
    voti = soup.find(
        'table', attrs={'class': 'griglia_tab', 'id': 'data_table_2'})
    rows = voti.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols if ele])
    if not voti:
        return jsonify("Tabella vuota o non trovata")
    return jsonify(data)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
